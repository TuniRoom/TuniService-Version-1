//
//  professionViewcontroller.swift
//  TuniService
//
//  Created by MacBook Pro on 07/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class professionViewcontroller:UIViewController ,UISearchBarDelegate  ,UICollectionViewDataSource, UICollectionViewDelegate{
    @IBOutlet weak var searchControllerView: UISearchBar!
     var stringSelectJob = "---- Select your job ----"
    @IBOutlet weak var tableView: UICollectionView!
    
    let tabStringJobs : [String] = ["Builder","Electrician","Electronics","Plumber","Taxi"]
    
    var filteredData  : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchControllerView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        self.filteredData = self.tabStringJobs
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        //Set delegate
       
    }
    
  
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return filteredData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = tableView.dequeueReusableCell(withReuseIdentifier: "cellProfession", for: indexPath)
        cell.layer.cornerRadius = 9
        cell.clipsToBounds = true
        
        
        let titleJob :UILabel = cell.viewWithTag(102) as! UILabel
        titleJob.text = filteredData[indexPath.row]
        
        let imageJob :UIImageView = cell.viewWithTag(101) as! UIImageView
        imageJob.image = UIImage(named: filteredData[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
       
        print("xxxxxxxxxxxx ",filteredData[indexPath.row].trimmingCharacters(in: .whitespacesAndNewlines))
        
        
    
        let data:[String: String] = ["text": filteredData[indexPath.row].trimmingCharacters(in: .whitespacesAndNewlines)]
     
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: data)
        
        self.removeAnimate()

    }
    
    @IBAction func back(_ sender: Any) {
        
        self.removeAnimate()

        
    }
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text intoqa the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredData = searchText.isEmpty ? tabStringJobs : tabStringJobs.filter { (item: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchControllerView.showsCancelButton = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchControllerView.showsCancelButton = false
        searchControllerView.text = ""
        searchControllerView.resignFirstResponder()
        filteredData = tabStringJobs
        tableView.reloadData()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
        
        
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
        
        
        
        
    }

}
