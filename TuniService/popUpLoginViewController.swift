 
 

 
    //
    //  popUpLoginViewController
    //  M-Dinar
    //
    //  Created by MacBook Pro on 10/07/2017.
    //  Copyright © 2017 Levelip. All rights reserved.
    //
    
    import UIKit
    import FirebaseAuth
    import FirebaseDatabase
    import DLRadioButton
    class popUpLoginViewController: UIViewController {
        
        @IBOutlet weak var txtEmail: UITextField!
        @IBOutlet weak var txtPassword: UITextField!
        
        @IBOutlet weak var btnLogin: UIButton!
        @IBOutlet weak var btnSignUp: UIButton!
        
        @IBOutlet weak var viewEmail: UIView!
        @IBOutlet weak var viewPassword: UIView!
        @IBOutlet weak var remeberMe: DLRadioButton!
        
        @IBOutlet var viewMain: UIView!
   
        
        @IBOutlet weak var v: UIView!
        
        @IBAction func RememberMeAction(_ sender: Any) {
            
            /* if (remeberMe.isSelected == true){
             remeberMe.selec
             }
             if (remeberMe.isSelected == false){
             remeberMe.isSelected = true
             }*/
            
        }
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
            
            self.remeberMe.isMultipleSelectionEnabled = true;
            
            // Make view with border
            viewEmail.layer.cornerRadius = 10
            viewEmail.layer.masksToBounds = true
            
            viewPassword.layer.cornerRadius = 10
            viewPassword.layer.masksToBounds = true
            
            v.layer.borderColor = UIColor.black.cgColor //set your color here
            //Start Animation
            self.showAnimate()
            
            
            
            //Exit
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(popUpLoginViewController.closepopup(_:)))
            viewMain.isUserInteractionEnabled = true
            viewMain.addGestureRecognizer(tapGestureRecognizer)
            
            
            
            
            // Set Data Email & password To Interface Login in case saved in shared preferences
            let Email : AnyObject = UserDefaults.standard.object(forKey: "Email") as AnyObject
            let Password: AnyObject = UserDefaults.standard.object(forKey: "Password") as AnyObject
            print("xxxxxxsdsdfsdf  ssdf sdf ds  \(Email)")
            if  ( (Email  == nil ) ||  ("\(Email)" == "<null>") ){
                txtEmail.text = ""
                txtPassword.text = ""
                remeberMe.isSelected = false
                
            }else {
                txtEmail.text = Email as? String
                txtPassword.text = Password as? String
                remeberMe.isSelected = true
                
            }
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        @IBAction func LoginAction(_ sender: Any) {
            
            let rootref = FIRDatabase.database().reference()
            var currentUser : User = User()
            FIRAuth.auth()?.signIn(withEmail: txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines), password: (txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines))!, completion: { (user, error) in
                
                
                if (user != nil ) { // C Bon
                    rootref.child("users").child((FIRAuth.auth()?.currentUser?.uid)!).observe(.value, with: { (snapshot) in
                       
                        //Get User From Firebase
                        
                        AppDelegate.CURRENT_USER = currentUser
                        
                       if ( self.remeberMe.isSelected == true ) {
                            // If succees save data in shared preferences
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(self.txtEmail.text!, forKey: "Email")
                            userDefaults.set(self.txtPassword.text!, forKey: "Password")
                        
                            //userDefaults.set(currentUser.uid, forKey: "uid")
                            userDefaults.synchronize()
                        }
                        let sb = Snackbar()
                        sb.sbLenght = .long
                        sb.createWithAction(text: "Welcome",
                                            actionTitle: "",
                                            action: {
                                                print("Button is push")
                        })
                        sb.show()
                        
                        
                        /************/
                    })
                    
                    self.removeAnimate()
                    
                    
                } else {
                    
                    
                    let sb = Snackbar()
                    sb.buttonColor = UIColor.red
                    sb.sbLenght = .long
                    
                    sb.createWithAction(text: "please check your Email and Password",
                                        actionTitle: "Erreur",
                                        action: {
                                            print("Button is push")
                    })
                    sb.show()
                    
                }
                
                
            })
            
            
            
            
            
            
            
            
            
        }
        @IBAction func SignUpAction(_ sender: Any) {
            
            
            
            
            
            
            
            /*let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpSignUpViewController") as! PopUpSignUpViewController
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.popupLoginViewController = self
            popOverVC.didMove(toParentViewController: self)
            */
            //
 
            
            
        }
        
        
        /*********** Close Pop up when click hors view login ******/
        func closepopup(_ sender:AnyObject){
            
            self.removeAnimate()
            
        }
        /*********** END Close Pop up when click hors view login ******/
        
        
        
        
        /********** Animation ************/
        
        
        func showAnimate()
        {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            UIView.animate(withDuration: 0.25, animations: {
                self.view.alpha = 1.0
                self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            });
        }
        
        func removeAnimate()
        {
            UIView.animate(withDuration: 0.25, animations: {
                self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
            });
        }
        
        /********** End Animation ************/
        
        
        
 }
 
