//
//  HomeViewController.swift
//  TuniService
//
//  Created by MacBook Pro on 03/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    @IBAction func yourRecentRequestAction(_ sender: Any) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "yourRecentRequest") as! yourRecentRequest
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)

        popOverVC.didMove(toParentViewController: self)
        
    }

}
