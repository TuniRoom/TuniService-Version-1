//
//  TuniHomeScreen.swift
//  TuniService
//
//  Created by MacBook Pro on 04/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit
import MapKit

class TuniHomeScreen: BaseViewController  , MKMapViewDelegate , CLLocationManagerDelegate {

    @IBOutlet weak var v: UIView!
    @IBOutlet weak var slideOut: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var mapView: MKMapView!
    /**************** Current Location ***************/
    
    let manager = CLLocationManager()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations[0]
        
        
        
        
        
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        
        
        
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        
        
        
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        
        
        
        mapView.setRegion(region, animated: false)
        
        
        
        print(location.altitude)
        print(location.speed)
        
        
        
        self.mapView.tintColor = UIColor.red
        
        
        self.mapView.showsUserLocation = true
        
    }
    /**************** End current location  ************/
    /******************* Change Color Pin **********
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        if annotation.isEqual(mapView.userLocation) {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "userLocation")
            annotationView.image = UIImage(named: "geo")
            return annotationView
        }
         
        return nil
     }
    ******************* End change color Pin **********/
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        addSlideMenuButton()
        manager.startUpdatingLocation()

        // Make pickture as circle
        bottomView.layer.cornerRadius = 9
        bottomView.layer.masksToBounds = true
        
        v.layer.cornerRadius = 9
        v.layer.masksToBounds = true
        //MAP
        /*********************** Start map ****************/
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        
        mapView.delegate = self
    
        
        
        
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        manager.startUpdatingLocation()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func slideOutAction(_ sender: Any) {
 
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TuniServices") as! TuniServices
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func currentLocationAction(_ sender: Any) {

    }
    
     func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: MKUserLocation.self){
            
            
        
        }
        
        return nil
    }
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        
        for childView:MKAnnotationView in views{
             if childView.annotation!.isKind(of: MKUserLocation.self){
                childView.canShowCallout = false
            }
        }
        
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation!.isKind(of: MKUserLocation.self){
            
            view.canShowCallout = false
            // инициализирую XIB
            let customView = (Bundle.main.loadNibNamed("ViewCallout", owner: self, options: nil))?[0] as! CustomCalloutView;
            
            
            let calloutViewFrame = customView.frame;
            
            customView.frame = CGRect(x: -calloutViewFrame.size.width/2.8, y: -calloutViewFrame.size.height-10, width: 269, height: 60)
            
            //A let cpa = view.annotation as! CustomPointAnnotation
            //A cpa.name = "ZZ"
            //A  customView.name.text = cpa.name
            view.clearsContextBeforeDrawing = true 
            view.addSubview(customView)
          

        }
        
        
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView)
    {
        print("Pin clicked");
        // скрываю меню того, что деселектируется
        for childView:AnyObject in view.subviews{
            childView.removeFromSuperview();
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        manager.stopUpdatingHeading()
        manager.stopUpdatingLocation()
    }
    
   
    
    @IBAction func currentlocationnAction(_ sender: Any) {
        
        // Set Data Email & password To Interface Login in case saved in shared preferences
        let Email : AnyObject = UserDefaults.standard.object(forKey: "Email") as AnyObject
        let Password: AnyObject = UserDefaults.standard.object(forKey: "Password") as AnyObject
        
        
        if ( "\(Email)"  == "<null>" ){
            
            
            
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpLoginViewController") as! popUpLoginViewController
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            
            
            
        }else{
            
            performSegue(withIdentifier: "goToCurrentLocation", sender: nil)
            
        }
    }

}
