//
//  ViewController.swift
//  TuniService
//
//  Created by MacBook Pro on 03/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    @IBOutlet weak var imageViewProfile: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        addSlideMenuButton()
        // Make pickture as circle
        imageViewProfile.layer.cornerRadius = imageViewProfile.frame.width/2
        imageViewProfile.layer.masksToBounds = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


  
    @IBAction func SendMonyAction(_ sender: Any) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpPayementSend") as! popUpPayementSend
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.viewNext = self.view
        popOverVC.viewControllerMainPayement = self
        popOverVC.didMove(toParentViewController: self)
    }
    
    
    @IBAction func RequestMonyAction(_ sender: Any) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpPaymentsRequest") as! popUpPaymentsRequest
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
}

