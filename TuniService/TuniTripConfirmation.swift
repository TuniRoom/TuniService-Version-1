//
//  TuniTripConfirmation.swift
//  TuniService
//
//  Created by MacBook Pro on 06/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit
import MapKit
class TuniTripConfirmation: UIViewController ,MKMapViewDelegate {

    @IBOutlet weak var ViewBottom: UIView!
    @IBOutlet weak var mapView: MapViewWithZoom!
    @IBOutlet weak var profile: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        zoomToRegion()

        // Do any additional setup after loading the view.
        ViewBottom.layer.cornerRadius = 9
        ViewBottom.clipsToBounds = true
        
        profile.layer.cornerRadius = profile.frame.width/2
        profile.layer.masksToBounds = true
        
        mapView.delegate = self
        
        mapView.isZoomEnabled = true
        
        let firstLoc = CLLocationCoordinate2DMake(-7.9333948, 112.6037314)
        // Создаем заколку
        let dropPin = CustomPointAnnotation(name: "rand", street: "rand", type: "rand", postCode: "rand");
        dropPin.coordinate = firstLoc
        dropPin.title = "Автомойка на Виноградской"
        dropPin.subtitle = "От 99 крон!!!"
        dropPin.imageName = "pickuppin"

        
        let firstLoc2 = CLLocationCoordinate2DMake(-7.93794, 112.606827)
        // Создаем заколку
        let dropPin2 = CustomPointAnnotation(name: "rand", street: "rand", type: "rand", postCode: "rand")
        dropPin2.coordinate = firstLoc2
        dropPin2.title = "Автомойка хер знает где"
        dropPin2.subtitle = "Цена по телефону, возможно!!!"
        dropPin2.imageName = "destinationpin"
        
  
        
        
        let sourcePlacemark = MKPlacemark(coordinate: firstLoc, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: firstLoc2, addressDictionary: nil)
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        
        
        // 6.
        self.mapView.showAnnotations([dropPin,dropPin2], animated: true )
        
        // 7.
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveLabels)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }

         
        
        /*********************** End map ****************/
        
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 2
        
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        print("delegate called")
        
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            // убрал дефолтную заколку
            anView!.canShowCallout = false
        }
        else {
            anView!.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        // проверка, если я ставлю заколку, которая является объектом класса КастомПоинтАннотейшн, то тогда я добавляю картинку
        // а если бы это был базовый аннотейшн, его бы не трогало и там бы шла обычная заколка дефолтная
        let cpa = annotation as! CustomPointAnnotation
        anView!.image = UIImage(named:cpa.imageName)
        
        
        
        return anView
    }
    
    //MARK:- Zoom to region
    
    func zoomToRegion() {
        
        // здесь вбил координаты центральных улиц, куда примерно смотреть
        let location = CLLocationCoordinate2D(latitude: 50.075538, longitude: 14.4378)
        // здесь
        let region = MKCoordinateRegionMakeWithDistance(location, 5000.0, 7000.0)
        
        mapView.setRegion(region, animated: true)
    }

}

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
    
    
    var name: String
    var street: String
    var type: String
    var postCode: String
    
    
    init(name: String, street: String, type: String, postCode: String) {
        self.name = name
        self.street = street
        self.type = type
        self.postCode = postCode
    }
    
    
    
}

