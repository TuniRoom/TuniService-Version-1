//
//  Pays.swift
//  TuniService
//
//  Created by MacBook Pro on 08/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class Pays: NSObject {

    
    var Street : String = ""
    var Country : String = ""
    var City : String = ""
    var State : String = ""
    
    init( dic : [AnyHashable : Any]){
        
        if (dic["Street"] != nil )  {
            Street = dic["Street"] as! String

        }
        if (dic["Country"] != nil )  {
            Country = dic["Country"] as! String
            
        }
        if (dic["City"] != nil )  {
            City = dic["City"] as! String
            
            }
        if (dic["State"] != nil )  {
            State = dic["State"] as! String
            
            }

    }
    
}
