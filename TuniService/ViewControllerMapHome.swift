 

import UIKit
import MapKit


 

class ViewControllerMapHome: UIViewController  , UIGestureRecognizerDelegate{
    var pays : Pays? = nil
    
    var selectedPin: MKPlacemark?
    var resultSearchController: UISearchController!
    var lat : Double = 0
    var lon : Double = 0
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func qsdqsd(_ sender: Any) {
        
        
        let data:[String: Pays] = ["text": pays!]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationNameSelectHOME"), object: nil, userInfo: data)
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let recognizer = UITapGestureRecognizer(target: self, action:#selector(ViewControllerMAP.Handle(_:)))
        recognizer.delegate = self
        mapView.addGestureRecognizer(recognizer)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController.searchResultsUpdater = locationSearchTable
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search for places"
        
        
        navigationItem.titleView = resultSearchController?.searchBar
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
        
    }
    
    func getDirections(){
        guard let selectedPin = selectedPin else { return }
        let mapItem = MKMapItem(placemark: selectedPin)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        mapItem.openInMaps(launchOptions: launchOptions)
    }
    @IBAction func Handle(_ sender: UITapGestureRecognizer) {
        mapView.removeAnnotations(mapView.annotations)
        
        
        
        let location = sender.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        lat = coordinate.latitude
        lon = coordinate.longitude
        mapView.clearsContextBeforeDrawing = true
        
        
        
        //TvPlace = ("\(cityy) \(countryy) \(streett)")
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
        
        
        
        lon = annotation.coordinate.longitude
        lat = annotation.coordinate.latitude
        print("xxxxx Fuck lan ",lon)
        print("xxxxx Fuck lat ",lat)
        if ( (lat != nil) && (lon != nil )) {
            let loc = CLLocation(latitude: lat, longitude: lon)
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(loc, completionHandler: { (placemarks, error) -> Void in
                // Place details
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                // Address dictionary
                self.pays = Pays(dic: placeMark.addressDictionary!)
                print(self.pays!.City)
                
            })
            
        }
        
        
        
        
    }
    
}

extension ViewControllerMapHome : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error)")
    }
    
}

extension ViewControllerMapHome: HandleMapSearch {
    
    func dropPinZoomIn(_ placemark: MKPlacemark){
        // cache the pin
        selectedPin = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
        print("in zoom")
        
        lon = annotation.coordinate.longitude
        lat = annotation.coordinate.latitude
        print("xxxxx Fuck lan ",lon)
        print("xxxxx Fuck lat ",lat)
        if ( (lat != nil) && (lon != nil )) {
            let loc = CLLocation(latitude: lat, longitude: lon)
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(loc, completionHandler: { (placemarks, error) -> Void in
                // Place details
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                // Address dictionary
                self.pays = Pays(dic: placeMark.addressDictionary!)
                print(self.pays!.City)
                
            })
            
        }
        
        
    }
    
}

extension ViewControllerMapHome : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        guard !(annotation is MKUserLocation) else { return nil }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        pinView?.pinTintColor = UIColor.orange
        pinView?.canShowCallout = true
        let smallSquare = CGSize(width: 30, height: 30)
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
        button.setBackgroundImage(UIImage(named: "car"), for: UIControlState())
        button.addTarget(self, action: #selector(ViewControllerMapHome.getDirections), for: .touchUpInside)
        pinView?.leftCalloutAccessoryView = button
        
        
        /* print("xxxxxxxxxxxxxxxxxxxx in mapvew view for annotation ")
         
         lon = annotation.coordinate.longitude
         lat = annotation.coordinate.longitude
         print("xxxxx Fuck lan ",lon)
         print("xxxxx Fuck lat ",lat)
         if ( (lat != nil) && (lon != nil )) {
         let loc = CLLocation(latitude: lat, longitude: lon)
         let geoCoder = CLGeocoder()
         geoCoder.reverseGeocodeLocation(loc, completionHandler: { (placemarks, error) -> Void in
         // Place details
         var placeMark: CLPlacemark!
         placeMark = placemarks?[0]
         // Address dictionary
         print(placeMark.addressDictionary)
         
         })
         
         }*/
        
        return pinView
    }
    
}
