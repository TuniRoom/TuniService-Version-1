//
//  yourRecentRequest.swift
//  TuniService
//
//  Created by MacBook Pro on 04/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit
import AARatingBar
import MapKit

class yourRecentRequest: UIViewController , MKMapViewDelegate {
    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet weak var ratingBar: AARatingBar!
    @IBOutlet weak var mapView: MKMapView!
 
    @IBOutlet weak var viewDone: UIView!
  
    @IBOutlet weak var btnNeedHelp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configRatingBar()

        // image profile
        // Make pickture as circle
        imageProfile.layer.cornerRadius = imageProfile.frame.width/2
        imageProfile.layer.masksToBounds = true
        
        
        // Button need help
        btnNeedHelp.backgroundColor = .clear
        btnNeedHelp.layer.cornerRadius = 5
        btnNeedHelp.layer.borderWidth = 1
        btnNeedHelp.layer.borderColor = UIColor.black.cgColor
        
        // MapView
        /*********************** Start map ****************/

        mapView.delegate = self
        let sourceLocation = CLLocationCoordinate2D(latitude: -7.9333948, longitude: 112.6037314)
        let destinationLocation = CLLocationCoordinate2D(latitude: -7.93794, longitude: 112.606827)
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "Times Square"
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = "Empire State Building"
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        // 6.
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        // 7.
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
        /*********************** End map ****************/
        
        
        
        viewDone.isHidden = true
        
        self.showAnimate()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePopUp(_ sender: AnyObject) {
        self.removeAnimate()
        //self.view.removeFromSuperview()
    }
    @IBAction func nextAction(_ sender: Any) {
        self.removeAnimate()
        
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
        
      
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func ratingBarInteraction(_ sender: UIButton) {
        
        ratingInteraction(sender)

        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 5.0
        
        return renderer
    }

    func configRatingBar() {
        ratingBar.ratingDidChange = { ratingValue in
            
            print("Rating Is Empty: ", self.ratingBar.isEmpty)
            self.viewDone.isHidden = false
        }
    }
    
    func ratingInteraction(_ sender: UIButton) {
        ratingBar.isEnabled = sender.isSelected
        
        if ratingBar.isEnabled {
            sender.setTitle("Click to disable!", for: .normal)
        }
        else {
            sender.setTitle("Click to enable!", for: .selected)
        }
        
        sender.isSelected = !sender.isSelected
    }
    @IBAction func doneAction(_ sender: Any) {
        self.removeAnimate()
    }

}
