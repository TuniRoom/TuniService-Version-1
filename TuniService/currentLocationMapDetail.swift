//
//  currentLocationMapDetail.swift
//  TuniService
//
//  Created by MacBook Pro on 05/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class currentLocationMapDetail: UIViewController  ,UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var viewss: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    let tabIcon1  = ["work","home_icon","Fill"]
    let tabIcon2  = ["Fill"]

    let tabTitle1 = ["Home","Work"]
    let tabSubTitle1 = ["Rue Hamouda Maali 1","La Venue Habib Bourghiba"]//
    let tabSubTitle2 = ["15 Rue de Carthage, Le 15, Tunis","N10, Carthage","2 Avenue Hedi Chaker"]//

    let tabTitle2 = ["Flat6Labs Tunis","Jobi","Tuni HQ"]
    override func viewDidLoad() {
        super.viewDidLoad()

        viewss.layer.cornerRadius = 9
        viewss.layer.masksToBounds = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ( section == 0 ) {
            return tabSubTitle1.count
        }
        else {
            return tabSubTitle2.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mycellHistory:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCellmapDetail")!

        
        let  v :UIImageView = mycellHistory.viewWithTag(101) as! UIImageView

        let  vv :UIView = mycellHistory.viewWithTag(200)!
        
        vv.layer.cornerRadius = 9
        vv.layer.masksToBounds = true
        
        
        mycellHistory.layer.cornerRadius = 9
        mycellHistory.layer.masksToBounds = true
        
        
        if (indexPath.section == 0 ){
            if ( indexPath.row == 0){
                
                v.image = UIImage(named: tabIcon1 [0])

            }else{
                
                v.image = UIImage(named: tabIcon1 [1])

            }
        } else {
            
            
                v.image = UIImage(named: tabIcon2 [0])
                
            
            
        }
     
        
        
        
                return mycellHistory
        
      
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v : UIView = UIView()
        v.backgroundColor = UIColor.clear
        return v
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "     "
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
