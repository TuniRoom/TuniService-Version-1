//
//  TuniServices.swift
//  TuniService
//
//  Created by MacBook Pro on 04/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class TuniServices: BaseViewController  ,UICollectionViewDataSource, UICollectionViewDelegate{

    var tab : [String ] = ["Builder" , "Electrician" , "Electronics" , "Plumber" , "Taxi" ] //
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        showAnimate()
        collectionView.delegate = self
        collectionView.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return tab.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCellService", for: indexPath)
        cell.layer.cornerRadius = 9
        cell.clipsToBounds = true
        
        let  title :UILabel = cell.viewWithTag(102) as! UILabel
        title.text = tab[indexPath.row]
        
        let imageI : UIImageView = cell.viewWithTag(101) as! UIImageView
        imageI.image = UIImage(named: tab[indexPath.row])

        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        // Set Data Email & password To Interface Login in case saved in shared preferences
        let Email : AnyObject = UserDefaults.standard.object(forKey: "Email") as AnyObject
        let Password: AnyObject = UserDefaults.standard.object(forKey: "Password") as AnyObject
        
        
        if ( "\(Email)"  == "<null>" ){
            
            
            
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpLoginViewController") as! popUpLoginViewController
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            
            
            
        }else{
            
            
            print("action")
            
            }
        
        
    }
        /*************/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
  
 
    
    func showAnimate()
    {
        UIView.animate(withDuration: 1, animations: {
            self.view.frame.origin.y = -(self.view.frame.maxY)
            
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 1, animations: {
            self.view.frame.origin.y = (self.view.frame.maxY)

        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.removeAnimate()

    }


}
