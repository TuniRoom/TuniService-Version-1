//
//  signupViewController.swift
//  TuniService
//
//  Created by MacBook Pro on 07/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit
import FoldingCell

class signupViewController: UIViewController  {
   
    

 
    
    @IBOutlet weak var buttonSelectJob: UIButton!
    @IBOutlet weak var buttoSelectHOMEAdd: UIButton!

    
    /******** Folding Cell *********/
    
    let DataSourcePlace =  ["home","work"]
    let DataSourcePlaceIcon =  [#imageLiteral(resourceName: "homemap"),#imageLiteral(resourceName: "workmapx")]
    @IBOutlet weak var tableView: UITableView!

    let kCloseCellHeight: CGFloat = 34
    let kOpenCellHeight: CGFloat = 488
    let kRowsCount = 2
    var cellHeights: [CGFloat] = []
    
    /********* End Folding Cell ****/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /******** Folding Cell *********/
        
         /********* End Folding Cell ****/
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheelSelectWork(_:)), name: NSNotification.Name(rawValue: "notificationNameSelectWork"), object: nil)
        //notificationNameSelectHOME
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheelSelectHOME(_:)), name: NSNotification.Name(rawValue: "notificationNameSelectHOME"), object: nil)

        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var buttoSelectWorkAdd: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func selectJobAction(_ sender: Any) {
   
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "professionViewcontroller") as! professionViewcontroller
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    func showSpinningWheel(_ notification: NSNotification) {
        
        print("Received Notification")
        buttonSelectJob.titleLabel?.text = notification.userInfo?["text"] as? String
        
        print("------- ",notification.userInfo?["text"] as! String)
    }
    
    func showSpinningWheelSelectWork(_ notification: NSNotification) {
        
        print("Received Notification2")

        buttoSelectWorkAdd.titleLabel?.text = (notification.userInfo?["text"] as? Pays)?.Country
        
        print("------- ",(notification.userInfo?["text"] as! Pays).Country)

    }
    //notificationNameSelectHOME
    func showSpinningWheelSelectHOME(_ notification: NSNotification) {
        
        print("Received notificationNameSelectHOME")
        
        buttoSelectHOMEAdd.titleLabel?.text = (notification.userInfo?["text"] as? Pays)?.Country
        
        print("------- ",(notification.userInfo?["text"] as! Pays).Country)
        
    }
    


}
extension UIButton {
    private struct AssociatedKeys {
        static var WithValue : Int?
    }
    
    @IBInspectable var withValue: Int? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.WithValue) as? Int
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.WithValue,
                    newValue as Int?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN
                )
            }
        }
    }
    
    private struct AssociatedKeysCell {
        static var WithValueCell : FoldingCell?
    }
    @IBInspectable var WithValueCell: FoldingCell? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeysCell.WithValueCell) as? FoldingCell
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeysCell.WithValueCell,
                    newValue as FoldingCell?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN
                )
            }
        }
    }
    //FoldingCell
}

