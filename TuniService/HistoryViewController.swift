//
//  HistoryViewController.swift
//  TuniService
//
//  Created by MacBook Pro on 03/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class HistoryViewController: BaseViewController ,UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mycellHistory:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "mycellHistory")!
        
        
        let  circle :UIImageView = mycellHistory.viewWithTag(101) as! UIImageView
        
        // Make pickture as circle
        circle.layer.cornerRadius = circle.frame.width/2
        circle.layer.masksToBounds = true
        
        return mycellHistory
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
    }


}
