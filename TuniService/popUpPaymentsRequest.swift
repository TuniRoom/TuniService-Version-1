//
//  popUpPaymentsRequest.swift
//  TuniService
//
//  Created by MacBook Pro on 03/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit

class popUpPaymentsRequest: UIViewController {
    
    
    
    @IBOutlet weak var viewcontroller: UIImageView!
    
    var  sampleTapGesture : UITapGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(popUpPaymentsRequest.closepopup(_:)))

        viewcontroller.isUserInteractionEnabled = true
        viewcontroller.addGestureRecognizer(tapGestureRecognizer)

        
        self.showAnimate()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func closepopup(_ sender:AnyObject){

        self.removeAnimate()
        //self.view.removeFromSuperview()
    
    }
 
  
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */
}
